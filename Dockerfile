# check http://pm2.keymetrics.io/docs/usage/docker-pm2-nodejs/ to run node under a process manager inside docker
FROM node:12-alpine

ENV NODE_ENV development

LABEL Luis Gomez

# Create app directory in container
RUN mkdir -p /app

# Set working directory
WORKDIR /app

# --no-cache: download package index on-the-fly, no need to cleanup afterwards
# --virtual: bundle packages, remove whole bundle at once, when done
# only copy package.json initially so that `RUN yarn` layer is recreated only
# if there are changes in package.json
COPY package.json package-lock.json pm2.json .env dist /app/

# --pure-lockfile: Don’t generate a yarn.lock lockfile
# Bundle app source
RUN apk --no-cache --virtual build-dependencies add \
    python \
    make \
    g++ \
    && npm install --production --silent \
    && apk del build-dependencies \
    && rm -f .npmrc \
    && npm install -g pm2

# Expose the docker port 80 and 443
EXPOSE 3000

# Start pm2 in docke mode
CMD ["pm2-runtime", "--json", "pm2.json"]
