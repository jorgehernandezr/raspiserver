import * as bcrypt from 'bcrypt'
import Usuarios from '../Modelos/Usuarios'
import { raw } from 'objection'
import BaseError from '../_errors/BaseError'
import * as HttpStatus from 'http-status'
import Token from '../Services/Token'
export default class UsuariosData {
  static async login(login: string, pass: string) {
    const data = await Usuarios.query()
      .where('login', login)
      .where(raw('password=sha1(?)', pass))
      .where('b_activo', 1)
      .first()
    if (data) {
      const token = await Token.generate({
        nombre: data.nombre,
        id_usuario: data.id_usuario
      })
      return { nombre: data.nombre, token: token }
    }
  }
}

export class TFiltroUsuarios {
  id_usuario: number
}
