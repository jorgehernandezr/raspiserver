import BaseError from '../_errors/BaseError'
import * as HttpStatus from 'http-status'
import UsuariosData from './UsuariosData'
import Usuarios from '../Modelos/Usuarios'

export default class UsuariosController {
  static async auth(req: any, res: any, next: any) {
    req
      .checkBody('login', 'Faltan parametro login')
      .notEmpty()
      .isLength({ min: 1, max: 100 })
    req
      .checkBody('password', 'Faltan parametro password')
      .notEmpty()
      .isLength({ min: 1, max: 100 })
    var errors = req.validationErrors()
    if (errors) {
      next(
        new BaseError(
          errors[0].param + ':' + errors[0].msg,
          HttpStatus.BAD_REQUEST
        )
      )
      return
    }
    try {
      const data = await UsuariosData.login(req.body.login, req.body.password)
      res.status(200).jsonp(data)
    } catch (ee) {
      //next
    }
  }
}
