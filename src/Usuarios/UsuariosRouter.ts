import * as express from 'express'
import UsuariosController from './UsuariosController'
import Token from '../Services/Token'
const UsuariosRouter = express.Router()

UsuariosRouter.post('/auth', UsuariosController.auth)

export default UsuariosRouter
