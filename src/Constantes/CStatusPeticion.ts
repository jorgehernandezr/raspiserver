enum CStatusPeticion {
  SOLICITADA = 'solicitada',
  ABIERTA = 'abierta',
  ERROR = 'error'
}
export default CStatusPeticion
