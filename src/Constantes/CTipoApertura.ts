enum CTipoApertura {
  ENTRADA = 'entrada',
  SALIDA = 'salida'
}

export default CTipoApertura
