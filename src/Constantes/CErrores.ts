class CErrores {
  static NO_ABRIO_PLUMA = {
    mensaje: 'No abrio la pluma', // se envío la señal, pero la pluma no responde
    status: 501
  }

  static NO_HAY_VEHICULO = {
    mensaje: 'No hay vehículo', // no hay presencia de vehiculo
    status: 502
  }

  static TARJETA_NO_ACCESIBLE = {
    mensaje: 'TARJETA NO ACCESIBLE', // la tarjeta no se encuentra conectada con el servidor
    status: 503
  }

  static TARJETA_DESCONECTADA = {
    mensaje: 'LA TARJETA SE DECONECTO', // se alcanzo a enviar la señar de apertura, pero se desconecto la ttarjeta
    status: 504
  }

  static NO_HAY_PESO = {
    mensaje: 'NO HAY PESO AL ABRIR',
    status: 505
  }
  static ERROR_CAJA_OCUPADA = {
    mensaje: 'CAJA OCUPADA',
    status: 506
  }
}

export default CErrores
