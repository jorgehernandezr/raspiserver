enum CStatusSincronizacionPeticion {
  PENDIENTE = 'pendiente',
  SINCRONIZADO = 'sincronizado'
}
export default CStatusSincronizacionPeticion
