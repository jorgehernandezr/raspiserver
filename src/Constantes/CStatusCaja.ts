enum CStatusCaja {
  CONECTADA = 'conectada',
  DESCONECTADA = 'desconectada'
}

export default CStatusCaja
