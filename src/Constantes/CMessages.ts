enum CMessages {
  OPEN = 'open', // solicitud para abrir la pluma
  OK_OPEN = 'ok_open', // indica que la pluma si se abrio
  ERROR_OPEN = 'error_open', // error al abrir la pluma

  REGISTRA_PETICION_ENTRADA = 'registrar_peticion_entrada',
  OK_REGISTRA_PETICION_ENTRADA = 'ok_registra_peticion_entrada',
  FIN_PETICIONES_ENTRADA = 'fin_peticiones_entrada',

  REGISTRA_PETICION_SALIDA = 'registra_peticion_cerrada',
  OK_REGISTRA_PETICION_SALIDA = 'ok_registra_peticion_cerrada',
  FIN_PETICIONES_SALIDA = 'fin_peticiones_salida',

  APERTURA_POR_QR = 'apertura_por_qr',
  OK_APERTURA_POR_QR = 'recibio_apertura_por_qr',

  ERROR_NO_HAY_PESO = 'no_hay_peso',

  ERROR_CAJA_OCUPADA = 'caja_ocupada',

  ERROR_RECIBIDO = 'error_recibido' // le informamos a la caja que se recibio un error de apertura
}

export default CMessages
