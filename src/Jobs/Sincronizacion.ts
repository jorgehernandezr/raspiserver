import * as CronJob from 'cron'
import Peticiones from '../Modelos/Peticiones'
import config from '../_env/config'
import * as request from 'request'
import { raw } from 'objection'
import BoxData from '../Box/BoxData'

export default class Sincronizacion {
  constructor() {
    const cron = new CronJob.CronJob(
      '*/300 * * * * *',
      () => {
        this.procesaSalidas()
      },
      function() {},
      true
    )
  }

  private async procesaSalidas() {
    await BoxData.sincronizaParkimovilSalidasQr()
  }
}

export interface TDatosSalidaQr {
  codigo_salida_qr: string
  fecha_salida: string
}

export interface TResultUrl {
  status: number // respuesta http
  respuesta: string // body que devuelve el webhook
}
