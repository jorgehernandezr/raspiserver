import * as HttpStatus from 'http-status'
export default class BaseError extends Error {
  public status: number = 0
  constructor(message: any, statusCode: any) {
    super(message)
    this.name = message
    this.message = message
    //@ts-ignore
    this.status = statusCode

    //Error.captureStackTrace(this, this.constructor.name);
  }
}
