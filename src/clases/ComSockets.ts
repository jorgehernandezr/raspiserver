import BaseError from '../_errors/BaseError'
import * as HttpStatus from 'http-status'
import * as uuidv4 from 'uuid/v4'
import BoxData, { TPeticionCaja } from '../Box/BoxData'

import CMessages from '../Constantes/CMessages'
import CErrores from '../Constantes/CErrores'
import Cajas from '../Modelos/Cajas'
import CStatusPeticion from '../Constantes/CStatusPeticion'
import Peticiones from '../Modelos/Peticiones'
import { raw } from 'objection'
import CTipoApertura from '../Constantes/CTipoApertura'
import Parkimovil from '../Services/Parkimovil'
import CStatusCaja from '../Constantes/CStatusCaja'
import logger from '../_env/logger'
import { query } from 'winston'

export default class ComSockets {
  private io: any
  private sockets: Array<TSocket> = new Array<TSocket>()
  private paginas: Array<TSocket> = new Array<TSocket>()
  private static instance: ComSockets = null

  static newComSocket(io: any) {
    if (ComSockets.instance == null) {
      ComSockets.instance = new ComSockets(io)
    } else {
      return ComSockets.instance
    }
  }
  private constructor(io: any) {
    this.io = io
    this.setListeners()
  }

  private setListeners() {
    let io = this.io

    io.on('connection', async (socket: any) => {
      logger.info('Se conecto ' + socket.handshake.query.tarjeta)

      if (socket.handshake.query.tarjeta) {
        //informamos a parkimovil que la caja está activa
        Parkimovil.cambioStatusCaja(
          socket.handshake.query.tarjeta,
          CStatusCaja.CONECTADA
        )

        const caja = await Cajas.query()
          .where('nombre', socket.handshake.query.tarjeta)
          .first()
        await Cajas.query()
          .patch({
            b_activa: 1,
            fecha_movimiento: new Date()
          })
          .where('nombre', socket.handshake.query.tarjeta)

        const elSocket = {
          numero_tarjeta: socket.handshake.query.tarjeta,
          socket: socket,
          status: TStatusSocket.ESPERA,
          id_remoto: '',
          id_caja: caja.id_caja,
          id_plaza: caja.id_plaza,
          id_primer_remoto: socket.handshake.query.id_primer_remoto,
          id_ultimo_remoto: socket.handshake.query.id_ultimo_remoto
        }
        this.sockets.push(elSocket)

        /**
         * Evento recibido si la pluma abrio correctamente
         */
        socket.on(CMessages.OK_OPEN, async (men: TRecepcionMensaje) => {
          logger.info(men.id_remoto + ' APERTURA ACORRECTA ')
          const socket2 = this.getSocket(men.caja)
          if (socket2) {
            // socket2.status = TStatusSocket.ESPERA;
          }
          await BoxData.siAbrio(men.id_remoto)

          //si el tipo de apertura fue de salida, informamos a las cajas que ya se cerro un ciclo
          const pet = await Peticiones.query()
            .where('id_remoto', men.id_remoto)
            .first()
          logger.info('el tipo es ' + pet.tipo)
          if (pet && pet.tipo == CTipoApertura.SALIDA) {
            await Peticiones.query()
              .patch({
                b_sincronizado: 1, // lo marcamos como ya sincronizado, pq fue una petición echa desde parkimovil
                fecha_salida_qr: new Date().toISOString().replace('T', ''), // marcamos la fecha de salida para que se sincronicen las demas caja
                id_peticion_salida: pet.id_peticion // relacionamos la peticion de entrada y la de salida
              })
              .where('id_ciclo', pet.id_ciclo)
              .where('tipo', CTipoApertura.ENTRADA)

            //informamos a las cajas que ya hubo una salida
            logger.info('Se informa de salida por peticion parkimovil')
            await this.sincronizaSalidasQr(socket2.id_caja)
          } else {
            //informamos a las demas cajas de una nueva apertura de entrada
            const cajaDestino = await Cajas.query()
              .where('nombre', men.caja)
              .first()
            if (cajaDestino) {
              const cajas = await Cajas.query().where(
                'id_plaza',
                cajaDestino.id_plaza
              )
              for (var i = 0; i < this.sockets.length; i++) {
                if (this.sockets[i].id_plaza == cajaDestino.id_plaza) {
                  this.sockets[i].socket.emit(
                    CMessages.REGISTRA_PETICION_ENTRADA,
                    {
                      id_remoto: pet.id_remoto,
                      codigo_salida_qr: pet.codigo_salida_qr, // código qr para la salida en caso de que no haya internet
                      caja: cajaDestino.nombre,
                      id_ciclo: pet.id_ciclo,
                      tipo: pet.tipo
                    } as TPeticionCaja
                  )
                }
              }
            } else {
              logger.error(
                men.id_remoto + ' ' + CErrores.TARJETA_NO_ACCESIBLE.mensaje
              )
              throw new BaseError(
                CErrores.TARJETA_NO_ACCESIBLE.mensaje,
                CErrores.TARJETA_NO_ACCESIBLE.status
              )
            }
          }
        })

        /**
         * La pluma NO abrio correctamente
         */
        socket.on(CMessages.ERROR_OPEN, async (datosError: TErrorApertura) => {
          logger.error(
            datosError.id_remoto + ' no abrio  ',
            datosError.tipo_error
          )

          const socket2 = this.getSocket(datosError.caja)

          if (datosError.tipo_error == CMessages.ERROR_OPEN) {
            await BoxData.noAbrio(
              datosError.id_remoto,
              CErrores.NO_ABRIO_PLUMA.mensaje,
              CErrores.NO_ABRIO_PLUMA.status
            )
            socket2.socket.emit(CMessages.ERROR_RECIBIDO, datosError.id_remoto)
          }
          if (datosError.tipo_error == CMessages.ERROR_NO_HAY_PESO) {
            await BoxData.noAbrio(
              datosError.id_remoto,
              CErrores.NO_HAY_PESO.mensaje,
              CErrores.NO_HAY_PESO.status
            )
            socket2.socket.emit(CMessages.ERROR_RECIBIDO, datosError.id_remoto)
          }
          if (datosError.tipo_error == CMessages.ERROR_CAJA_OCUPADA) {
            await BoxData.noAbrio(
              datosError.id_remoto,
              CErrores.ERROR_CAJA_OCUPADA.mensaje,
              CErrores.ERROR_CAJA_OCUPADA.status
            )
            socket2.socket.emit(CMessages.ERROR_RECIBIDO, datosError.id_remoto)
          }
        })

        /**
         * La caja nos informa que la sincronización de una peticion se realizo corractmente
         */
        socket.on(
          CMessages.OK_REGISTRA_PETICION_ENTRADA,
          async (recepcion: TRecepcionMensaje) => {
            const socket2 = this.getSocket(recepcion.caja)
            if (socket2) {
              logger.info(
                'confirma sincronizacion ' +
                  socket2.id_caja +
                  '(' +
                  socket2.numero_tarjeta +
                  ') = ' +
                  recepcion.id_remoto
              )
              const hayPendientes = await this.sincronizaPeticionesEntrada(
                socket2,
                recepcion.id_remoto,
                true
              )
              socket2.id_ultimo_remoto = recepcion.id_remoto
              if (!hayPendientes) {
                this.sincronizaPeticionesSalida(
                  socket2,
                  socket.id_primer_remoto,
                  true
                )
              }
            }
          }
        )

        socket.on(
          CMessages.OK_REGISTRA_PETICION_SALIDA,
          async (recepcion: TRecepcionMensaje) => {
            const socket2 = this.getSocket(recepcion.caja)
            if (socket2) {
              logger.info(
                'confirma sincronizacion ' +
                  socket2.id_caja +
                  '(' +
                  socket.numero_tarjeta +
                  ') = ' +
                  recepcion.id_remoto
              )
              const hayPendientes = await this.sincronizaPeticionesSalida(
                socket2,
                recepcion.id_remoto,
                false
              )
              socket2.id_ultimo_remoto = recepcion.id_remoto
              if (!hayPendientes) {
                socket2.socket.emit(CMessages.FIN_PETICIONES_SALIDA)
              }
            }
          }
        )

        /**
         * Se confirmo la sincronización de apertura por qr
         */
        socket.on(
          CMessages.APERTURA_POR_QR,
          async (recepcionSalidaQr: TRecepcionSalidaQr) => {
            logger.info(
              'se recibe confirmación de salida por qr',
              recepcionSalidaQr
            )
            const socket = this.getSocket(recepcionSalidaQr.caja)
            const caja = await Cajas.query()
              .where('nombre', recepcionSalidaQr.caja)
              .first()
            await Peticiones.query()
              .patch({
                id_caja_salida_qr: caja.id_caja,
                fecha_salida_qr: recepcionSalidaQr.fecha_salida
                  .substr(0, 10)
                  .replace('T', '')
              })
              .where('id_remoto', recepcionSalidaQr.id_remoto)
            socket.socket.emit(CMessages.OK_APERTURA_POR_QR, {
              id_remoto: recepcionSalidaQr.id_remoto
            })
            this.sincronizaSalidasQr(caja.id_caja, recepcionSalidaQr.id_remoto)
          }
        )

        // avisamos a la página de control de cajas, sobre una actualización
        await this.avisaActualizacionCaja()

        // verificamos si hay peticiones abierta
        const hayPendientes = await this.sincronizaPeticionesEntrada(
          elSocket,
          '',
          true
        )
        if (!hayPendientes) {
          logger.info(
            'NO HAY PENDIENTES Y POR ENDE INFORMAMOS DE PETICIONES DE SALIDA'
          )
          const hayPendientes = await this.sincronizaPeticionesSalida(
            elSocket,
            '',
            false
          )
          if (!hayPendientes) {
            socket.emit(CMessages.FIN_PETICIONES_SALIDA)
          }
        }

        await this.sincronizaSalidasQr(caja.id_caja)

        socket.on('disconnect', async () => {
          for (var i = 0; i < this.sockets.length; i++) {
            if (this.sockets[i].socket == socket) {
              //buscamos las peticiones que se quedaron como pendientes y que no alcanzo a responder la caja
              const solicitadas = await Peticiones.query()
                .where('qr', this.sockets[i].numero_tarjeta)
                .where('status', CStatusPeticion.SOLICITADA)
              for (var k = 0; k < solicitadas.length; k++) {
                BoxData.noAbrio(
                  solicitadas[k].id_remoto,
                  CErrores.TARJETA_DESCONECTADA.mensaje,
                  CErrores.TARJETA_DESCONECTADA.status
                )
              }
              logger.info(
                '----- DESCONECTADO ' + this.sockets[i].numero_tarjeta
              )

              Parkimovil.cambioStatusCaja(
                this.sockets[i].numero_tarjeta,
                CStatusCaja.DESCONECTADA
              )

              await Cajas.query()
                .patch({
                  b_activa: 0,
                  fecha_movimiento: new Date()
                })
                .where('nombre', this.sockets[i].numero_tarjeta)

              this.avisaActualizacionCaja()
              //this.sockets[i].status == TStatusSocket.ESPERA;
              this.sockets.splice(i, 1)
              return
            }
          }
        })
      } else {
        const data = await Cajas.query().orderBy('nombre')
        this.paginas.push({
          numero_tarjeta: '',
          socket: socket,
          //status: TStatusSocket.ESPERA,
          id_remoto: '',
          id_caja: 0,
          id_plaza: 0,
          id_primer_remoto: '',
          id_ultimo_remoto: ''
        })
        socket.emit('actualizacion', data)
      }
    })
  }

  private async avisaActualizacionCaja() {
    const data = await Cajas.query().orderBy('nombre')
    for (var i = 0; i < this.paginas.length; i++) {
      this.paginas[i].socket.emit('actualizacion', data)
    }
  }

  private getSocket(caja: string) {
    for (var i = 0; i < this.sockets.length; i++) {
      if (this.sockets[i].numero_tarjeta == caja) {
        return this.sockets[i]
      }
    }
    return null
  }

  /**
   * Envia la solicitud al socket de que abra la pluma
   * @param tarjeta
   * @param webhooks
   * @param exitCode
   */
  public async msgOpen(
    tarjeta: string,
    idPlaza: number,
    datosPeticion: TPeticionCaja
  ) {
    //verificamos si la tarjeta destino está conectada
    let tarjeta_conectada = false
    for (var i = 0; i < this.sockets.length; i++) {
      if (this.sockets[i].numero_tarjeta == tarjeta) {
        tarjeta_conectada = true
        break
      }
    }
    if (!tarjeta_conectada) {
      throw new BaseError(
        CErrores.TARJETA_NO_ACCESIBLE.mensaje,
        CErrores.TARJETA_NO_ACCESIBLE.status
      )
    }

    //enviamos la señal de apertura a todas las cajas de la misma plaza
    for (var i = 0; i < this.sockets.length; i++) {
      if (
        this.sockets[i].id_plaza == idPlaza &&
        this.sockets[i].numero_tarjeta == datosPeticion.caja
      ) {
        if (this.sockets[i].numero_tarjeta == datosPeticion.caja) {
          //this.sockets[i].status = TStatusSocket.EN_APERTURA;
          this.sockets[i].id_remoto = datosPeticion.id_remoto
        }
        logger.info(
          'si se envio emit(open) a la caja' + this.sockets[i].numero_tarjeta
        )
        await this.sockets[i].socket.emit(CMessages.OPEN, datosPeticion)
        //return;
      }
    }
  }

  total() {
    return this.sockets.length
  }

  /**
   * sincroniza las peticiones de apertura que no tenga la caja
   */
  private async sincronizaPeticionesEntrada(
    socket: TSocket,
    idUltimoRemoto: string,
    sendNext: boolean
  ) {
    return new Promise(async (resolve, reject) => {
      let peticion = null

      if (idUltimoRemoto == undefined || idUltimoRemoto == '') {
        peticion = await Peticiones.query()
          .whereRaw('DATEDIFF(CURDATE(),fecha)<=1')
          .first()
        sendNext = true
      } else {
        peticion = await Peticiones.query()
          .where('id_remoto', idUltimoRemoto)
          .first()
      }

      if (peticion) {
        const data = Peticiones.query()
          .innerJoin('cajas', 'cajas.nombre', 'peticiones.qr')
          .innerJoin('plazas', 'plazas.id_plaza', 'cajas.id_plaza')
        if (sendNext) {
          data.where(
            raw(
              "peticiones.status!='" +
                CStatusPeticion.ERROR +
                "' AND peticiones.status!='" +
                CStatusPeticion.SOLICITADA +
                "' AND id_peticion>? " +
                " AND id_peticion_salida is null AND tipo='" +
                CTipoApertura.ENTRADA +
                "' AND fecha_salida_qr IS NULL ",
              [peticion.id_peticion]
            )
          )
        } else {
          data.where(
            raw(
              "peticiones.status!='" +
                CStatusPeticion.ERROR +
                "'  AND peticiones.status!='" +
                CStatusPeticion.SOLICITADA +
                "' AND id_peticion=? " +
                " AND id_peticion_salida is null AND tipo='" +
                CTipoApertura.ENTRADA +
                "' AND fecha_salida_qr IS NULL ",
              [peticion.id_peticion]
            )
          )
        }
        const pendientes = await data.first()
        if (pendientes) {
          resolve(true)
          logger.info(
            'Se envia peticion de entrada ' +
              pendientes.id_remoto +
              ' a la caja ' +
              socket.numero_tarjeta
          )
          socket.socket.emit(CMessages.REGISTRA_PETICION_ENTRADA, {
            id_remoto: pendientes.id_remoto,
            codigo_salida_qr: pendientes.codigo_salida_qr,
            caja: pendientes.qr,
            id_ciclo: pendientes.id_ciclo,
            tipo: pendientes.tipo
          } as TPeticionCaja)
        } else {
          resolve(false)
        }
      } else {
        resolve(false)
      }
    })
  }

  /**
   * Sincronizamos las peticiones que ya tienen salida
   */
  private async sincronizaPeticionesSalida(
    socket: TSocket,
    idRemoto: string,
    esPrimerRemoto: boolean
  ) {
    return new Promise(async (resolve, reject) => {
      let peticion = null
      if (idRemoto == undefined || idRemoto == '') {
        peticion = await Peticiones.query()
          .whereRaw('DATEDIFF(CURDATE(),fecha)<=1')
          .first()
        esPrimerRemoto = true
      } else {
        peticion = await Peticiones.query()
          .where('id_remoto', idRemoto)
          .first()
      }
      if (peticion) {
        const data = Peticiones.query()
          .innerJoin('cajas', 'cajas.nombre', 'peticiones.qr')
          .innerJoin('plazas', 'plazas.id_plaza', 'cajas.id_plaza')
        if (esPrimerRemoto) {
          data.where(
            raw(
              "peticiones.status!='" +
                CStatusPeticion.ERROR +
                "' AND id_peticion=? AND not id_peticion_salida is null",
              [peticion.id_peticion]
            )
          )
        } else {
          data.where(
            raw(
              "peticiones.status!='" +
                CStatusPeticion.ERROR +
                "' AND id_peticion>? AND not id_peticion_salida is null ",
              [peticion.id_peticion]
            )
          )
        }
        const pendientes = await data
          .where('tipo', CTipoApertura.ENTRADA)
          .first()
        if (pendientes) {
          resolve(true)

          socket.socket.emit(CMessages.REGISTRA_PETICION_SALIDA, {
            id_remoto: pendientes.id_remoto,
            codigo_salida_qr: pendientes.codigo_salida_qr,
            caja: pendientes.qr,
            id_ciclo: pendientes.id_ciclo,
            tipo: pendientes.tipo
          } as TPeticionCaja)
        } else {
          resolve(false)
        }
      } else {
        resolve(false)
      }
    })
  }

  /**
   * sincroniza las salidas por QR
   * @param caja
   */
  public async sincronizaSalidasQr(idCaja: number, idRemoto?: string) {
    const plaza = await Cajas.query()
      .where('id_caja', idCaja)
      .first()
    const sql = Peticiones.query()
      .eager('caja')
      .innerJoin('cajas', 'cajas.nombre', 'peticiones.qr')
      .whereRaw(
        `NOT peticiones.fecha_salida_qr IS NULL AND tipo='${CTipoApertura.ENTRADA}' `
      )
      .where('cajas.id_plaza', plaza.id_plaza)
    if (idRemoto && idRemoto != '') {
      sql.where('peticiones.id_remoto', idRemoto)
    }
    const peticiones = (await sql) as Array<Peticiones>
    logger.info(
      'mandamos a sincroniza un total de ' + peticiones.length + ' salida '
    )

    for (var i = 0; i < peticiones.length; i++) {
      for (var k = 0; k < this.sockets.length; k++) {
        if (this.sockets[k].id_plaza == peticiones[i].caja.id_plaza) {
          logger.info(
            peticiones[i].id_remoto +
              ' se manda como salida por a ' +
              this.sockets[k].numero_tarjeta
          )
          this.sockets[k].socket.emit(
            CMessages.APERTURA_POR_QR,
            peticiones[i].id_remoto
          )
        }
      }
    }
  }

  /**
   * Devuelve el socket conectado a X caja
   */
  public cajaDisponible(caja: string) {
    for (var i = 0; i < this.sockets.length; i++) {
      if (this.sockets[i].numero_tarjeta == caja) {
        return this.sockets[i]
      }
    }
    return null
  }
}

/**
 * Cada que una caja/cliente se conecta se guarda un objeto de la clase TSocket
 */
export class TSocket {
  numero_tarjeta: string
  id_caja: number
  id_plaza: number
  //status: TStatusSocket;
  socket: any
  id_remoto: string
  id_primer_remoto: string
  id_ultimo_remoto: string
}
export enum TStatusSocket {
  ESPERA = 'espera',
  EN_APERTURA = 'en_apertura'
}

export class TErrorApertura {
  id_remoto: string
  tipo_error: CMessages
  caja: string
}

export class TRecepcionMensaje {
  id_remoto: string
  caja: string
  id_ultimo_remoto: string
}

export class TRecepcionSalidaQr {
  id_remoto: string
  caja: string
  fecha_salida: string
}
