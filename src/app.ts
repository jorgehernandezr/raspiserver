import * as bodyParser from 'body-parser'
import BaseError from './_errors/BaseError'
import config from './_env/config'
import UsuariosRouter from './Usuarios/UsuariosRouter'
import ComSockets from './clases/ComSockets'
import UsuariosController from './Usuarios/UsuariosController'
import BoxRouter from './Box/BoxRouter'
import Sincronizacion from './Jobs/Sincronizacion'
import Box from './Box/BoxData'
import logger from './_env/logger'

//server.js
class App {
  public app: any
  public http: any
  public sockets: any

  constructor() {
    this.app = require('express')()
    this.http = require('http').Server(this.app)
    var io = require('socket.io')(this.http)

    this.sockets = ComSockets.newComSocket(io) // conexion de sockets
    this.config()
    this.mountRoutes()
    this.manejaErrores()
    this.scanner()
  }
  scanner() {
    /*var usb = require('usb');
    console.log(usb.getDeviceList());*/
  }

  config() {
    this.app.use(bodyParser.json())
    this.app.use(bodyParser.urlencoded({ extended: true }))

    this.app.use(function(req: any, res: any, next: any) {
      res.setHeader('Access-Control-Allow-Origin', '*')

      var allowedOrigins = ['http://localhost']
      var origin = req.headers.origin
      if (allowedOrigins.indexOf(origin) > -1) {
        res.setHeader('Access-Control-Allow-Origin', origin)
      }

      // Request methods you wish to allow
      res.setHeader(
        'Access-Control-Allow-Methods',
        'GET, POST, OPTIONS, PUT, PATCH, DELETE'
      )

      // Request headers you wish to allow
      res.setHeader(
        'Access-Control-Allow-Headers',
        'X-Requested-With,content-type'
      )
      res.setHeader('Access-Control-Allow-Credentials', 'true')
      res.setHeader(
        'Access-Control-Allow-Headers',
        'Content-Type, Content-Length, X-Requested-With,u'
      )
      next()
    })
  }

  private mountRoutes(): void {
    this.app.use('/parki', BoxRouter)
    this.app.use('/parki', UsuariosRouter)
  }

  private manejaErrores() {
    this.app.use((err: any, req: any, res: any, next: any) => {
      if (config.env === 'development') {
        let status = parseFloat(err.status) || 500
        if (status != 200) {
          if (status == 142) status = 400
          if (err instanceof BaseError) {
            let error = {
              name: err.name,
              status,
              message: err.message,
              stack: err.stack
            }
            res.status(status).jsonp(error)
            logger.error('Error maneo de errores', err)
          } else {
            let error = {
              name: 'Error al realizar esta tarea 1',
              status,
              code: err.code,
              message: 'Error al realizar esta tarea',
              stack: err.stack + err.traza ? err.traza : ''
            }
            logger.error('Error maneo de errores', err)
            res.status(status).jsonp(error)
          }
        }
      } else {
        const status = err.status || 500
        if (status === 500) {
          logger.error('Error maneo de errores', err)
          err.message = 'Internal Server Error'
        }
        if (status != 200) {
          if (err instanceof BaseError) {
            let error = {
              name: err.name,
              status,
              message: err.message
            }
            logger.error('Error maneo de errores', err)
            res.status(status).jsonp(error)
          } else {
            let error = {
              name: 'Error al realizar esta tarea 2',
              status: err.code,
              code: err.code,
              message: err.message
            }
            logger.error('Error maneo de errores', err)
            res.status(status).jsonp(error)
          }
        }
      }
    })
  }
}

new App().http.listen(3000, async function() {
  new Sincronizacion()
  //await Box.sincronizaCajasSalidaQr();
  logger.info('listening on *:3000')
})
