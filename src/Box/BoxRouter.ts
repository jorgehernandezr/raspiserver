import * as express from 'express'
import BoxController from './BoxController'
import Token from '../Services/Token'
const BoxRouter = express.Router()

/**
 * @api get open Permite solicitar una apertura
 */

/** recive la solicitud de apertura de una pluma */
BoxRouter.post('/open', BoxController.open)

/** Lista el status de las caja */
BoxRouter.get('/boxes', [Token.checkToken], BoxController.getCajas)

/** de pruebas */
//BoxRouter.post("/webhook",BoxController.webhookPrueba);

export default BoxRouter
