import * as HttpStatus from 'http-status'
import BaseError from '../_errors/BaseError'
import BoxData, { TPeticion } from './BoxData'
import Cajas from '../Modelos/Cajas'
import Plazas from '../Modelos/Plazas'

export default class BoxController {
  /**
   * @api post /open Solicita la apertura de una pluma
   * @group Apertura Solicitar apertura
   *
   * @param box string nombre de la pluma a abrir
   * @param id string identificador unico de la solicitud de apertura
   * @param codigo_salida_qr string código qr que se usará para registrar una salida en caso de no haber conexión
   * @param webhooks_apertura Array<string> endpoints donde se responde si la pluma abrió o no. La respuesta se envía por post
   * @param webhooks_sincronizacion Array<string> endpoints donde que se llama para sincronizar una salida por lectura de qr
   * @param id_ciclo string identificador unico del ciclo (entrada-salida)
   * @param tipo string entrada|salida
   *
   * @code
   * //ejemplo de llamada
   * {
   *  "box":"E1010",
   *  "id":"peticion1",
   *  "codigo_salida_qr":"codigo1",
   *  "webhooks_apertura":["https://www.google.com"],
   *  "webhooks_sincronizacion":["https://www.google.com"],
   *  "id_ciclo":"ciclo1",
   *  "tipo":"entrada"
   * }
   *
   * Código de apertura correcta
   * {
   *  status:200,
   *  mensaje:"Apertura correcta",
   *  id:"13123-1232322-12332323", // id de la petición
   * }
   *
   * Códigos de error
   * {
   *  status:501,
   *  mensaje:"No abrio la pluma",
   *  id:"13123-1232322-12332323", // id de la petición
   * }
   * {
   *  status:503,
   *  mensaje:"TARJETA NO ACCESIBLE", // la caja se encuentra desconectada
   *  id:"13123-1232322-12332323", // id de la petición
   * }
   * {
   *  status:504,
   *  mensaje:"LA TARJETA SE DECONECTO", // se envío la petición a la tarjeta, pero en el proceso se desconecto
   *  id:"13123-1232322-12332323", // id de la petición
   * }
   * {
   *  status:505,
   *  mensaje:"NO HAY PESO",
   *  id:"13123-1232322-12332323", // id de la petición
   * }
   * {
   *  status:506,
   *  mensaje:"CAJA OCUPADA", // la caja está atendiendo una solicitud de apertura previa
   *  id:"13123-1232322-12332323", // id de la petición
   * }
   *
   */
  static async open(req: any, res: any, next: any) {
    req
      .checkBody('box', 'Faltan parametro box')
      .notEmpty()
      .isLength({ min: 1, max: 10 })
    req
      .checkBody('id', 'Faltan parametro id')
      .notEmpty()
      .isLength({ min: 1, max: 100 })
    req
      .checkBody('webhooks_apertura', 'Faltan parametro webhooks_apertura')
      .notEmpty()
      .isArray()
    req
      .checkBody(
        'webhooks_sincronizacion',
        'Faltan parametro webhooks_sincronizacion'
      )
      .notEmpty()
      .isArray()
    req
      .checkBody('codigo_salida_qr', 'Faltan parametro codigo_salida_qr')
      .notEmpty()
      .isLength({ min: 1, max: 40 })
    req
      .checkBody('id_ciclo', 'Falta parametro id_ciclo')
      .notEmpty()
      .isLength({ min: 1, max: 40 })
    req
      .checkBody('tipo', 'Falta parametro tipo')
      .notEmpty()
      .isLength({ min: 1, max: 30 })

    var errors = req.validationErrors()
    if (errors) {
      next(
        new BaseError(
          errors[0].param + ':' + errors[0].msg,
          HttpStatus.BAD_REQUEST
        )
      )
      return
    }

    try {
      const peticion: TPeticion = {
        qr: req.body.box,
        id_remoto: req.body.id,
        webhooksApertura: req.body.webhooks_apertura,
        webhooksSincronizacion: req.body.webhooks_sincronizacion,
        codigo_salida_qr: req.body.codigo_salida_qr,
        id_ciclo: req.body.id_ciclo,
        tipo: req.body.tipo
      }

      BoxData.open(peticion)
      res.status(200).jsonp({ status: 1 })
    } catch (ee) {
      next(ee)
    }
  }

  /**
   * @api get boxes/ Devuelve un listdo de las cajas con su status
   *
   * @code
   * //Ejemplo de respuesta
   * [{
   *  nombre:'E1010',
   *  b_activa:1, // 1|0 si está o no conectada actualmente
   *  fecha_movimiento:'2019-01-01 10:32:00', fecha y hora de su ultima conexion/deconexion
   *  id_plaza:1 // id de la plaza a la que pertenece
   * },...]
   */
  static async getCajas(req: any, res: any, next: any) {
    try {
      const data = await Cajas.query().orderBy('nombre')
      res.status(200).jsonp(data)
    } catch (ee) {
      next(ee)
    }
  }
}
