import ComSockets from '../clases/ComSockets'
import Peticiones from '../Modelos/Peticiones'
import BaseError from '../_errors/BaseError'
import CStatusPeticion from '../Constantes/CStatusPeticion'
import * as request from 'request'
import LogWebhooks from '../Modelos/LogWebhooks'
import CErrores from '../Constantes/CErrores'
import Cajas from '../Modelos/Cajas'
import { raw } from 'objection'
import * as HttpStatus from 'http-status'
import CSSincronizacionPeticion from '../Constantes/CStatusSincronizacionPeticion'
import CTipoApertura from '../Constantes/CTipoApertura'
import Parkimovil from '../Services/Parkimovil'
import logger, * as Logger from '../_env/logger'

export default class Box {
  static async open(peticion: TPeticion) {
    try {
      logger.info(peticion.id_remoto + ' NUEVA PETICION  ', { data: peticion })
      // guardamos la petición
      const data = await Peticiones.query()
        .insert({
          qr: peticion.qr,
          fecha: new Date(),
          id_remoto: peticion.id_remoto,
          webhooks_apertura: JSON.stringify(peticion.webhooksApertura),
          webhooks_sincronizacion: JSON.stringify(
            peticion.webhooksSincronizacion
          ),
          codigo_salida_qr: peticion.codigo_salida_qr,
          id_ciclo: peticion.id_ciclo,
          tipo: peticion.tipo
        })
        .returning('*')

      //verificamos si la caja esta conectada
      const sockets = ComSockets.newComSocket(null)
      if (sockets.cajaDisponible(peticion.qr) == null) {
        ///logger.error(peticion.id_remoto+ " "+CErrores.TARJETA_NO_ACCESIBLE.mensaje,peticion);
        throw new BaseError(
          CErrores.TARJETA_NO_ACCESIBLE.mensaje,
          CErrores.TARJETA_NO_ACCESIBLE.status
        )
      }
      // verificamos de que plaza es
      const cajaDestino = await Cajas.query()
        .where('nombre', peticion.qr)
        .first()
      if (cajaDestino) {
        //solicitamos la apertura
        await sockets.msgOpen(peticion.qr, cajaDestino.id_plaza, {
          id_remoto: peticion.id_remoto,
          codigo_salida_qr: peticion.codigo_salida_qr,
          caja: cajaDestino.nombre,
          id_ciclo: peticion.id_ciclo,
          tipo: peticion.tipo
        })
      } else {
        throw new BaseError(
          CErrores.TARJETA_NO_ACCESIBLE.mensaje,
          CErrores.TARJETA_NO_ACCESIBLE.status
        )
      }
    } catch (ee) {
      logger.error(ee, { data: peticion })
      //guardamos el error en la peticion
      await Peticiones.query()
        .patch({
          error_mensaje: ee.message,
          error_status: ee.status,
          status: CStatusPeticion.ERROR
        })
        .where('id_remoto', peticion.id_remoto)

      //informamos a parkimovil del error de apertura
      await Parkimovil.sendAperturaIncorrecta(
        peticion.id_remoto,
        CErrores.TARJETA_NO_ACCESIBLE.mensaje,
        CErrores.TARJETA_NO_ACCESIBLE.status
      )
    }
  }

  /**
   * Registra la apertura de lapluma e informamos a Parkimovil
   */
  static async siAbrio(idRemoto: string) {
    await Peticiones.query()
      .patch({
        status: CStatusPeticion.ABIERTA
      })
      .where('id_remoto', idRemoto)

    logger.info(
      'Se abrio la pluma --------------- ' +
        idRemoto +
        ' APERTURA CORRECTA 200 '
    )

    await Parkimovil.sendAperturaCorrecta(idRemoto)
  }

  /**
   * Registra la no apertura de la pluma e informamos a Parkimovil
   */
  static async noAbrio(idRemoto: string, mensaje: string, status: number) {
    await Peticiones.query()
      .patch({
        status: CStatusPeticion.ERROR,
        error_mensaje: mensaje,
        error_status: status
      })
      .where('id_remoto', idRemoto)
    logger.error('se manda un error de apertura')
    await Parkimovil.sendAperturaIncorrecta(idRemoto, mensaje, status)
  }

  /**
   * Verifica si hay salidar por qr que no se han informado a parkimovil y las envía
   */
  public static async sincronizaParkimovilSalidasQr() {
    return new Promise(async (resolve, reject) => {
      resolve()
      const data = await Peticiones.query().where(
        raw('not fecha_salida_qr is null AND b_sincronizado=0')
      )
      logger.info('Comienza sincronización de salidas a parkimovil')

      for (var i = 0; i < data.length; i++) {
        const webs = JSON.parse(data[i].webhooks_sincronizacion)
        for (var k = 0; k < webs.length; k++) {
          const envio = await Box.sendWebSincronizacion(
            webs[k],
            data[i].codigo_salida_qr,
            data[i].fecha_salida_qr,
            data[i]
          )

          await Peticiones.query()
            .patch({
              status_sincronizacion: envio.status,
              mensaje_sincronizacion: envio.respuesta,
              b_sincronizado: envio.status == 200 ? 1 : 0
            })
            .where('id_peticion', data[i].id_peticion)
        }
      }
    })
  }

  /**
   * Hace el llamado al webhook de parkimovil informando de una salida por qr
   * @param ruta
   * @param codigoSalidaQr
   * @param fechaSalidaISO
   */
  public static async sendWebSincronizacion(
    ruta: string,
    codigoSalidaQr: string,
    fechaSalidaISO: string,
    peticion: Peticiones
  ) {
    return new Promise<TResultWebhook>(async (resolve, reject) => {
      var options = {
        method: 'POST',
        url: ruta,
        headers: {
          'Cache-Control': 'no-cache',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          codigo_salida_qr: codigoSalidaQr,
          fecha_salida: new Date(fechaSalidaISO).getTime()
        })
      }
      const log = await LogWebhooks.query()
        .insert({
          url: ruta,
          id_peticion: peticion.id_peticion,
          id_remoto: peticion.id_remoto,
          mensaje: 'AVISO DE APERTURA POR QR',
          fecha: new Date()
        })
        .returning('*')
      try {
        request(options, async function(error: any, response: any, body: any) {
          if (error) {
            logger.info('en error ', error)
            await LogWebhooks.query()
              .patch({
                status: error.code,
                mensaje: error.message
              })
              .where('id_log', log.id_log)
            resolve({ status: error.code, respuesta: error.message })
            return
          }
          let message = ''
          try {
            body = JSON.parse(body)
            message = body.message == undefined ? '' : body.message
          } catch (ee) {}
          logger.info('ok', message)
          await LogWebhooks.query()
            .patch({
              status: response.statusCode,
              status_respuesta: message
            })
            .where('id_log', log.id_log)

          resolve({
            status: response.statusCode,
            respuesta: response.statusMessage + ':' + message + '-'
          })
        })
      } catch (ee) {
        logger.error('Error al llamar al webhook', ee)
        resolve({ status: 500, respuesta: 'Error' })
      }
    })
  }

  /**
   * Verificamos si el codigo_salida_qr existe y no ha sido usado
   */
  public static async getPeticion(codigoSalidaQr: string) {
    const pet = await Peticiones.query()
      .where('codigo_salida_qr', codigoSalidaQr)
      .where(raw('fecha_salida_qr is null'))
      .first()
    if (pet) {
      return pet
    } else {
      throw new BaseError('Codigo no existente', HttpStatus.NOT_FOUND)
    }
  }

  /**
   * Les informamos a las cajas que hubo una salida por Qr
   */
  /*public static async sincronizaCajasSalidaQr(){
        ComSockets.newComSocket(null).sincronizaSalidasQr();
    }*/
}
export interface TResultWebhook {
  status: number // respuesta http
  respuesta: string // body que devuelve el webhook
}

export class TPeticion {
  id_remoto: string // identificador unico de la solicitud según el app parkimovil
  qr: string // nombre de la caja
  webhooksApertura: string[]
  webhooksSincronizacion: string[]
  codigo_salida_qr: string // codigo de salida qr en caso de que no haya conexión (a la salida)
  id_ciclo: string // identificador unico del ciclo entrada/salida
  tipo: CTipoApertura
}

export class TPeticionCaja {
  id_remoto: string // identificador unico de la solicitud según el app parkimovil
  codigo_salida_qr: string // código qr para la salida en caso de que no haya internet
  caja: string
  id_ciclo: string
  tipo: CTipoApertura
}
