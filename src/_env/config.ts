import * as Joi from '@hapi/joi'

require('dotenv').config()

// define validation for all the env vars
const envVarsSchema = Joi.object({
  NODE_ENV: Joi.string()
    .allow(['development', 'production', 'test', 'sandbox'])
    .default('development'),
  PORT: Joi.number().default(8081),
  DB_HOST: Joi.string()
    .required()
    .description('MySQL DB host url'),
  DB_NAME: Joi.string()
    .required()
    .description('MySQL DB name'),
  DB_USER: Joi.string()
    .required()
    .description('MySQL DB user'),
  DB_POOL_MAX: Joi.number().default(10),
  DIAS_RECURRENCIA: Joi.number().default(3),
  JWT_SECRET: Joi.string()
    .required()
    .description('JWT server secret required.'),
  WEBHOOK_CAMBIO_STATUS_PARKIMOVIL: Joi.string()
    .required()
    .description('Webhook del aviso de cambio de status de una caja'),
  LOGGER: Joi.boolean().default(true)
})
  .unknown()
  .required()
const { error, value: envVars } = Joi.validate(process.env, envVarsSchema)
if (error) {
  throw new Error(`Error en el archivo de configuración: ${error.message}`)
}

const config = {
  env: envVars.NODE_ENV,
  port: envVars.PORT,
  database:
    envVars.NODE_ENV == 'development'
      ? {
          client: 'mysql',
          connection: {
            host: envVars.DB_HOST,
            user: envVars.DB_USER,
            password: envVars.DB_PASSWORD,
            database: envVars.DB_NAME,
            charset: 'utf8',
            timezone: 'UTC'
          }
          /* pool: {
        min: 0,
        max: envVars.DB_POOL_MAX
      }, */
          /*debug: envVars.DEBUG_FLAG*/
        }
      : {
          client: 'mysql',
          connection: {
            host: envVars.DB_HOST_SERVER,
            user: envVars.DB_USER_SERVER,
            password: envVars.DB_PASSWORD_SERVER,
            database: envVars.DB_NAME_SERVER,
            charset: 'utf8',
            timezone: 'UTC'
          }
          /*  pool: {
        min: 0,
        max: envVars.DB_POOL_MAX
      }, */
        },
  emailAccount: {
    host: envVars.email_host, // 'smtp.sendgrid.net',
    port: envVars.email_port, //587,
    secure: false, // true for 465, false for other ports
    auth: {
      user: envVars.email_user, //"apikey", // generated ethereal user
      pass: envVars.email_pass // "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" // generated ethereal password
    }
  },
  jwtSecret: envVars.JWT_SECRET,
  ruta_tmp: './tmp/',
  servidor:
    envVars.NODE_ENV == 'development'
      ? envVars.SERVIDOR_LOCAL
      : envVars.SERVIDOR_REMOTO,
  webhook_cambio_status_parkimovil: envVars.WEBHOOK_CAMBIO_STATUS_PARKIMOVIL,
  logger: envVars.LOGGER
}

export default config
