import * as winston from 'winston'
import * as moment from 'moment-timezone'
import { createLogger, format, transports } from 'winston'
const { combine, timestamp, label, prettyPrint, printf } = format
const myFormat = printf((info: any) => {
  /*  if(typeof info.message === "object"){
    info.message=JSON.stringify(info.message);
  } */
  return (
    moment().format('YYYY-MM-DD HH:mm:ss') +
    ':' +
    (info.level ? info.level : '') +
    ' : ' +
    (info.name ? info.name : '') +
    ' : ' +
    (info.message ? info.message : '') +
    ' : ' +
    (info.stack ? info.stack : '') +
    ' : ' +
    (info.data ? JSON.stringify(info.data) : '') +
    (info.message && info.message.stack ? info.message.stack : '') +
    ' : '
  )
})

const otroDato = printf((info: any) => {
  try {
    return '' //JSON.stringify(info);
  } catch (ee) {}
})
var logger = winston.createLogger({
  transports: [
    new winston.transports.File({
      level: 'debug',
      filename: './logs/log.log',
      handleExceptions: true,
      maxsize: 5242880, //5MB
      maxFiles: 5,
      //colorize: false,
      format: combine(timestamp(), myFormat)
    }),
    new winston.transports.Console({
      level: 'debug',
      handleExceptions: true,
      format: combine(timestamp(), winston.format.colorize(), myFormat)
    })
  ],
  exceptionHandlers: [new transports.File({ filename: './logs/log.log' })],
  exitOnError: false
})
export default logger
