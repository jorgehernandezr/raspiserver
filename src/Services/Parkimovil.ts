import CStatusCaja from '../Constantes/CStatusCaja'
import * as request from 'request'
import LogWebhooks from '../Modelos/LogWebhooks'
import Peticiones from '../Modelos/Peticiones'
import BaseError from '../_errors/BaseError'
import config from '../_env/config'
import LogStatusCajas from '../Modelos/LogStatusCajas'
import Cajas from '../Modelos/Cajas'
import logger from '../_env/logger'

export default class Parkimovil {
  /**
   * Se informa a parkimovil que si abrio la pluma
   * @param idRemoto
   */
  public static async sendAperturaCorrecta(idRemoto: string) {
    const data = await Peticiones.query()
      .where('id_remoto', idRemoto)
      .first()
    const webs = JSON.parse(data.webhooks_apertura)
    for (var i = 0; i < webs.length; i++) {
      await this.sendWebHook(
        webs[i],
        data.id_peticion,
        new BaseError('Apertura correcta', 200)
      )
    }
  }

  /**
   * Se informa a parkimovil que nno se habrío la pluma
   * @param idRemoto
   * @param mensaje
   * @param status
   */
  public static async sendAperturaIncorrecta(
    idRemoto: string,
    mensaje: string,
    status: number
  ) {
    const data = await Peticiones.query()
      .where('id_remoto', idRemoto)
      .first()

    const webs = JSON.parse(data.webhooks_apertura)
    for (var i = 0; i < webs.length; i++) {
      await this.sendWebHook(
        webs[i],
        data.id_peticion,
        new BaseError(mensaje, status)
      )
    }
  }

  //avisamos que cambio el status de una caja
  public static async cambioStatusCaja(caja: string, status: CStatusCaja) {
    //actualizamos mi status
    const laCaja = await Cajas.query()
      .where('nombre', caja)
      .first()
    await LogStatusCajas.query().insert({
      id_caja: laCaja.id_caja,
      id_plaza: laCaja.id_plaza,
      b_activo: status == CStatusCaja.CONECTADA ? 1 : 0,
      fecha: new Date()
    })

    const token = this.getToken()
    this.send(config.webhook_cambio_status_parkimovil, 'post', token, {
      box: caja,
      status: status
    })
  }

  private static getToken() {
    return ''
  }

  private static send(url: string, metodo: string, token: string, data: any) {
    return new Promise(async (resolve, reject) => {
      var options = {
        method: metodo,
        url: url,
        headers: {
          'Cache-Control': 'no-cache',
          'Content-Type': 'application/json',
          u: token
        },
        json: true,
        body: data
      }
      try {
        const log = await LogWebhooks.query()
          .insert({
            fecha: new Date(),
            url: url,
            mensaje: JSON.stringify(data)
          })
          .returning('*')
        request(options, async function(error: any, response: any, body: any) {
          if (error) {
            logger.error('error al enviar a parkimovil ', { data: error })
            await LogWebhooks.query()
              .patch({
                status: error.code,
                mensaje: error.message
              })
              .where('id_log', log.id_log)
            resolve({ status: error.code, respuesta: error.message })
            return
          }
          let message = ''
          try {
            body = JSON.parse(body)
            message = body.message == undefined ? '' : body.message
          } catch (ee) {}

          await LogWebhooks.query()
            .patch({
              status: response.statusCode,
              status_respuesta: message
            })
            .where('id_log', log.id_log)

          resolve({
            status: response.statusCode,
            respuesta: response.statusMessage + ':' + message + '-'
          })
        })
      } catch (ee) {
        logger.info('Error al llamar al webhook', { data: ee })
        resolve({ status: 500, respuesta: 'Error' })
      }
    })
  }

  /**
   * Permite hacer informar al API parkimovil sobre la apertura de la pluma
   * @param ruta
   * @param mensaje
   */
  private static async sendWebHook(
    ruta: string,
    idPeticion: number,
    statusDevolver: BaseError
  ) {
    const data = await Peticiones.query()
      .where('id_peticion', idPeticion)
      .first()
    //@ts-ignore
    statusDevolver.id = data.id_remoto
    var options = {
      method: 'POST',
      url: ruta,
      headers: {
        'Cache-Control': 'no-cache',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(statusDevolver)
    }

    const web = await LogWebhooks.query()
      .insert({
        url: ruta,
        id_peticion: idPeticion,
        id_remoto: data.id_remoto,
        mensaje: statusDevolver.message,
        status: statusDevolver.status,
        fecha: new Date()
      })
      .returning('*')

    try {
      request(options, async function(error: any, response: any, body: any) {
        if (error) {
          await LogWebhooks.query()
            .patch({
              status_respuesta: 'error'
            })
            .where('id_log', web.id_log)
          logger.error('ERROR AL LLAMAR AL WEBHOOK', error)
          return
        }
        await LogWebhooks.query()
          .patch({
            status_respuesta: 'ok'
          })
          .where('id_log', web.id_log)
      })
    } catch (ee) {
      logger.error('Error al llamar al webhook', { data: ee })
    }
  }
}
