import * as crypto from 'crypto'
import config from '../_env/config'
export default class Crypto {
  static async cifrar(text: string) {
    try {
      var mykey = crypto.createCipher('aes-128-cbc', config.jwtSecret)
      var mystr = mykey.update(text, 'utf8', 'hex')
      mystr += mykey.final('hex')
      return mystr
    } catch (ee) {
      console.log(ee)
      return ''
    }
  }
  static async descifrar(text: string) {
    var decipher = crypto.createDecipher('aes-128-cbc', config.jwtSecret)
    var dec = decipher.update(text, 'hex', 'utf8')
    dec += decipher.final('utf8')
    return dec
  }
}

//module.exports = Crypto;
