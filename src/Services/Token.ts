import * as JWT from 'jsonwebtoken'
import config from '../_env/config'
import * as HttpStatus from 'http-status'
import BaseError from '../_errors/BaseError'
import Usuarios from '../Modelos/Usuarios'
/**
 * Administra el token de session
 * @param {string} tk el token a aministrar
 */

export default class Token {
  static isValid(tk: any) {
    try {
      const token = JWT.verify(tk, config.jwtSecret, { maxAge: '43200' })
      return true
    } catch (err) {
      return false
    }
  }

  static getPayLoad(tk: any) {
    if (Token.isValid(tk)) return JWT.decode(tk)
    else return {}
  }
  static generate(payload: any) {
    return JWT.sign(payload, config.jwtSecret, { expiresIn: '43200000' })
  }

  static async checkToken(req: any, res: any, next: any) {
    if (!req.headers.u) {
      console.log('No tiene cabecera')
      return res
        .status(403)
        .send({
          status: HttpStatus.UNAUTHORIZED,
          message: '1)Tu petición no tiene cabecera de autorización'
        })
    }
    try {
      var token = req.headers.u
      JWT.verify(
        token,
        config.jwtSecret,
        { maxAge: '43200000' },
        async (error, data: any) => {
          if (error) {
            console.log('Usuario no permitido')
            next(
              new BaseError(
                '1)Usuario no permitido o sesión caducada',
                HttpStatus.UNAUTHORIZED
              )
            )
            return
          } else {
            if (data.login && data.password) {
              const usuarios = await Usuarios.query()
                .where('login', data.login)
                .whereRaw('password=sha1(?)', [data.password])
              req.user = usuarios[0]
              req.user.id_sucursal = data.id_sucursal
              next()
            }
          }
        }
      )
    } catch (ee) {
      console.log(ee)
      return res
        .status(403)
        .send({
          status: HttpStatus.UNAUTHORIZED,
          message: '2) Tu petición no tiene cabecera de autorización'
        })
    }
  }
}

//module.exports=Token;
