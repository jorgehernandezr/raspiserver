export default class Utils {
  static toDate(fecha: Date) {
    const val = new Date().getTimezoneOffset()
    fecha.setHours(fecha.getHours() - val / 60)
    return fecha
  }

  static dateToStr(fecha: Date) {
    return (
      Utils.to2(fecha.getDate()) +
      '/' +
      Utils.to2(fecha.getMonth() + 1) +
      '/' +
      fecha.getFullYear()
    )
  }

  static to2(dia: number) {
    if (dia < 10) return '0' + dia
    else return '' + dia
  }
}
