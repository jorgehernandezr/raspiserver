import { Model } from 'objection'
import conexion from './_conexion'

Model.knex(conexion)

export default class Plazas extends Model {
  public id_plaza: number
  public descripcion: string

  static get tableName() {
    return 'plazas'
  }
  static get idColumn() {
    return 'id_plaza'
  }
  /*static get relationMappings(){
            return {
    arjetas:{
                elation:Model.HasManyRelation,
                modelClass:ClientesTarjetas,
                join:{
                from:'clientes.id_cliente',
                    to:'clientes_tarjetas.id_cliente'
                }
            }
        }
    }*/
}
