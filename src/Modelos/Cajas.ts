import { Model } from 'objection'
import conexion from './_conexion'
import Plazas from './Plazas'

Model.knex(conexion)

export default class Cajas extends Model {
  public id_caja: number
  public nombre: string
  public detalles: string
  public b_activa: number
  public fecha_movimiento: Date
  public id_plaza: number

  public plaza: Plazas

  static get tableName() {
    return 'cajas'
  }
  static get idColumn() {
    return 'id_caja'
  }
  static get relationMappings() {
    return {
      plaza: {
        relation: Model.HasOneRelation,
        modelClass: Plazas,
        join: {
          from: 'cajas.id_plaza',
          to: 'plazas.id_plaza'
        }
      }
    }
  }
}
