import { Model } from 'objection'
import conexion from './_conexion'

Model.knex(conexion)

export default class Usuarios extends Model {
  public id_usuario: number
  public login: string
  public password: string
  public b_activo: number
  public nombre: string

  static get tableName() {
    return 'usuarios'
  }
  static get idColumn() {
    return 'id_usuario'
  }
  static get relationMappings() {
    return {}
  }
}
