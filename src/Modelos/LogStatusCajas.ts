import { Model } from 'objection'
import conexion from './_conexion'

Model.knex(conexion)

export default class LogStatusCajas extends Model {
  public id_log: number
  public id_caja: number
  public id_plaza: number
  public b_activo: number
  public fecha: Date

  static get tableName() {
    return 'log_status_cajas'
  }
  static get idColumn() {
    return 'id_log'
  }
  /*static get relationMappings(){
            return {
    arjetas:{
                elation:Model.HasManyRelation,
                modelClass:ClientesTarjetas,
                join:{
                from:'clientes.id_cliente',
                    to:'clientes_tarjetas.id_cliente'
                }
            }
        }
    }*/
}
