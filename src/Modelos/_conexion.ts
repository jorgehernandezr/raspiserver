import config from '../_env/config'
import * as client from 'knex'

const conexion = client(config.database)

export default conexion
