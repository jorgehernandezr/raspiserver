import { Model } from 'objection'
import conexion from './_conexion'

Model.knex(conexion)

export default class Log extends Model {
  public id_log: number
  public descripcion: string
  public fecha: Date

  static get tableName() {
    return 'log'
  }
  static get idColumn() {
    return 'id_log'
  }
  /*static get relationMappings(){
            return {
    arjetas:{
                elation:Model.HasManyRelation,
                modelClass:ClientesTarjetas,
                join:{
                from:'clientes.id_cliente',
                    to:'clientes_tarjetas.id_cliente'
                }
            }
        }
    }*/
}
