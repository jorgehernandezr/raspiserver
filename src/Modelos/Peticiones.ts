import { Model } from 'objection'
import conexion from './_conexion'
import CStatusPeticion from '../Constantes/CStatusPeticion'
import CTipoApertura from '../Constantes/CTipoApertura'
import Cajas from './Cajas'

Model.knex(conexion)

export default class Peticiones extends Model {
  public id_peticion: number
  public qr: string
  public id_remoto: string
  public webhooks_apertura: string
  public webhooks_sincronizacion: string
  public codigo_salida_qr: string
  public fecha: Date
  public error_mensaje: string
  public error_status: number
  public status: CStatusPeticion
  public id_ciclo: string
  public tipo: CTipoApertura

  public fecha_salida_qr: string // fecha de salida según tarjeta/codigo_salida_qr
  public status_sincronizacion: number
  public mensaje_sincronizacion: string
  public b_sincronizado: number
  public id_caja_salida_qr: number
  public id_peticion_salida: number

  public caja: Cajas

  static get tableName() {
    return 'peticiones'
  }
  static get idColumn() {
    return 'id_peticion'
  }
  static get relationMappings() {
    return {
      caja: {
        relation: Model.HasOneRelation,
        modelClass: Cajas,
        join: {
          from: 'peticiones.qr',
          to: 'cajas.nombre'
        }
      }
    }
  }
}
