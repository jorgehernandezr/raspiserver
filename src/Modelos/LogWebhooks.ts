import { Model } from 'objection'
import conexion from './_conexion'

Model.knex(conexion)

export default class LogWebhooks extends Model {
  public id_log: number
  public url: string
  public mensaje: string
  public status: number
  public status_respuesta: string
  public id_peticion: number
  public id_remoto: string
  public fecha: Date

  static get tableName() {
    return 'log_webhooks'
  }
  static get idColumn() {
    return 'id_log'
  }
  /*static get relationMappings(){
            return {
    arjetas:{
                elation:Model.HasManyRelation,
                modelClass:ClientesTarjetas,
                join:{
                from:'clientes.id_cliente',
                    to:'clientes_tarjetas.id_cliente'
                }
            }
        }
    }*/
}
